import React, { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [valid, setValid] = useState(false);
    const [alertMSG, setAlertMSG] = useState(false);
    const history = useHistory();

    useEffect(() => {
        const authToken = localStorage.getItem('Email');
        if (authToken) {
            history.push('/users-list');
        }
    });
    const onClick = (e) => {
        e.preventDefault();
        if (email === '' || password === '') {
            setValid(true);
        } else if (email === 'admin@demo.com' && password === 'Admin@123') {
            localStorage.setItem('Email', JSON.stringify(email));
            history.push('/users-list');
        } else {
            setAlertMSG(true);
        }
    };
    return (
        <div className="container bg-info text-center" style={{ width: '30%' }}>
            {alertMSG ? (
                <div className="alert alert-danger" role="alert">
                    Please Enter Valid Login!
                </div>
            ) : (
                ''
            )}
            <form>
                <div className="mb-3 fs-1">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                        Email address
                    </label>
                    <input type="email" id="exampleInputEmail1" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                    {valid ? <span className="text-danger">email required!</span> : ''}
                </div>
                <div className="mb-3 fs-1">
                    <label htmlFor="id" className="form-label">
                        Password
                    </label>
                    <input type="password" id="id" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
                    {valid ? <span className="text-danger">password required!</span> : ''}
                </div>
                <button className="btn btn-primary mb-2" onClick={onClick}>
                    Submit
                </button>
            </form>
        </div>
    );
};

export default LoginForm;
