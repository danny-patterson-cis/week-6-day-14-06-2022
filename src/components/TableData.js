import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Data from '../data.json';
import { isValidArray } from '../Modules/utils';
import '../assets/styles/TableData.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleDown, faArrowCircleUp } from '@fortawesome/free-solid-svg-icons';

const TableData = () => {
    const history = useHistory();
    const [newData, setNewData] = useState();
    const [pages, setPages] = useState(1);
    const [dataPerPage, setDataPerPage] = useState(10);
    const [globalSearch, setGlobalSearch] = useState('');
    const [name, setName] = useState('');
    const [gender, setGender] = useState('');
    const [age, setAge] = useState('');
    const [company, setCompany] = useState('');
    const [address, setAddress] = useState('');
    const [balance, setBalance] = useState('');
    const [date, setDate] = useState('');
    const [status, setStatus] = useState('');
    const lastShowData = pages * dataPerPage;
    const initialShowData = lastShowData - dataPerPage;

    const [order, setOrder] = useState({
        name: 'ASC',
        gender: 'ASC',
        age: 'ASC',
        company: 'ASC',
        address: 'ASC',
        balance: 'ASC',
        registered: 'ASC',
    });

    const sorting = (col, keyValue) => {
        let newOrder = order;
        if (col === 'ASC') {
            const sorted = [...Data].sort((a, b) => {
                if (a[keyValue].toString().toLowerCase() === b[keyValue].toString().toLowerCase()) {
                    return b[keyValue].toString().toLowerCase() - a[keyValue].toString().toLowerCase();
                }
                return a[keyValue].toString().toLowerCase() > b[keyValue].toString().toLowerCase() ? 1 : -1;
            });
            setNewData(sorted);
            newOrder[keyValue] = 'DSC';
            setOrder(newOrder);
        }
        if (col === 'DSC') {
            const sorted = [...Data].sort((a, b) => {
                if (a[keyValue].toString().toLowerCase() === b[keyValue].toString().toLowerCase()) {
                    return b[keyValue].toString().toLowerCase() - a[keyValue].toString().toLowerCase();
                }
                return a[keyValue].toString().toLowerCase() < b[keyValue].toString().toLowerCase() ? 1 : -1;
            });
            setNewData(sorted);
            newOrder[keyValue] = 'ASC';
            setOrder(newOrder);
        }
    };
    const getFilterData = (data = Data) => {
        if (!globalSearch) {
            return data
                .filter((d) => {
                    if (name) {
                        return d?.name.toLowerCase().includes(name.toLowerCase());
                    }
                    return true;
                })
                .filter((d) => {
                    if (gender) {
                        return d?.gender.toLowerCase() === gender.toLowerCase();
                    }
                    return true;
                })
                .filter((d) => {
                    if (age) {
                        return d?.age.toString().includes(age.toLowerCase());
                    }
                    return true;
                })
                .filter((d) => {
                    if (company) {
                        return d?.company.toLowerCase() === company.toLowerCase();
                    }
                    return true;
                })
                .filter((d) => {
                    if (address) {
                        return d?.address.toLowerCase().includes(address.toLowerCase());
                    }
                    return true;
                })
                .filter((d) => {
                    if (balance) {
                        return d?.balance.toLowerCase().includes(balance.toLowerCase());
                    }
                    return true;
                })
                .filter((d) => {
                    if (date) {
                        return d?.registered.toLowerCase().includes(date.toLowerCase());
                    }
                    return true;
                })
                .filter((d) => {
                    if (status) {
                        return d?.isActive.toString() === status.toLowerCase();
                    }
                    return true;
                });
        } else {
            return data.filter((d) => {
                return (
                    d.name.toLowerCase().includes(globalSearch.toLowerCase()) ||
                    String(d.age).toLowerCase() === globalSearch ||
                    d.company.toLowerCase().includes(globalSearch.toLowerCase()) ||
                    d.gender.toLowerCase() === globalSearch.toLowerCase() ||
                    d.balance.toLowerCase().includes(globalSearch.toLowerCase()) ||
                    d.isActive.toString() === globalSearch
                );
            });
        }
    };

    const filterData = getFilterData(newData);

    const getListData = () => {
        if (isValidArray(filterData)) {
            return filterData.slice(initialShowData, lastShowData);
        }
        return [];
    };
    const listData = getListData();

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(filterData.length / dataPerPage); i++) {
        pageNumbers.push(i);
    }
    const logout = (e) => {
        e.preventDefault();
        localStorage.removeItem('Email');
        history.push('/');
    };
    useEffect(() => {
        const authToken = localStorage.getItem('Email');
        if (!authToken) {
            history.push('/');
        }
    });
    useEffect(() => {
        setPages(1);
    }, [globalSearch, name, age, gender, company, address, date, balance, status]);

    return (
        <div className="body">
            <div className="container">
                <div className="logout pt-3 mb-5">
                    <button className="btn btn-danger" onClick={logout}>
                        Logout
                    </button>
                </div>
            </div>
            <div>
                <div className="entry-data">
                    Show
                    <select className="form-select" onChange={(e) => setDataPerPage(e.target.value)}>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    Entries
                </div>
                <div className="globalSearch mb-5 text-center">
                    Search:{' '}
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Search here"
                        value={globalSearch}
                        onChange={(e) => setGlobalSearch(e.target.value)}
                    />
                </div>
            </div>
            <div>
                <div className="tableData">
                    <table className="table table-striped text-center">
                        <thead className="border-bottom-dark">
                            <tr className="table table-striped">
                                <th scope="col" onClick={() => sorting(order.name, 'name')}>
                                    Name {order.name === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.gender, 'gender')}>
                                    Gender {order.gender === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.age, 'age')}>
                                    Age {order.age === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.company, 'company')}>
                                    Company{' '}
                                    {order.company === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.address, 'address')}>
                                    Address{' '}
                                    {order.address === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.balance, 'balance')}>
                                    Balance{' '}
                                    {order.balance === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col" onClick={() => sorting(order.registered, 'registered')}>
                                    Date{' '}
                                    {order.registered === 'ASC' ? <FontAwesomeIcon icon={faArrowCircleUp} /> : <FontAwesomeIcon icon={faArrowCircleDown} />}
                                </th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {listData.length > 0 ? (
                                listData.map((d) => {
                                    return (
                                        <tr className="table table-striped" key={d.index}>
                                            <td>{d.name}</td>
                                            <td>{d.gender}</td>
                                            <td>{d.age}</td>
                                            <td>{d.company}</td>
                                            <td>{d.address}</td>
                                            <td>{d.balance}</td>
                                            <td>{d.registered.slice(0, 10)}</td>
                                            <td className="text-center">
                                                <p className={d.isActive ? 'text-success' : 'text-danger'}>{d.isActive ? 'Active' : 'Deactivated'}</p>
                                            </td>
                                        </tr>
                                    );
                                })
                            ) : (
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td className="text-danger">No Data To Show!</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            )}
                            <tr>
                                <td>
                                    <div className="text-center">
                                        Name Search
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Name"
                                            value={name}
                                            onChange={(e) => setName(e.target.value)}
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Select Gender
                                        <select className="form-select" value={gender} onChange={(e) => setGender(e.target.value)}>
                                            <option value="">Select Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Age Search
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Age"
                                            value={age}
                                            onChange={(e) => setAge(e.target.value, 'age')}
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Select Company
                                        <select
                                            className="form-select"
                                            value={company}
                                            onChange={(e) => {
                                                setCompany(e.target.value);
                                            }}
                                        >
                                            <option value="">Select Company</option>
                                            {[...Data]
                                                .sort((a, b) => (a.company > b.company ? 1 : -1))
                                                .map((d) => {
                                                    return (
                                                        <option value={d.company} key={d.index}>
                                                            {d.company}
                                                        </option>
                                                    );
                                                })}
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Address Search
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Address"
                                            value={address}
                                            onChange={(e) => setAddress(e.target.value)}
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Balance Search
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Balance"
                                            value={balance}
                                            onChange={(e) => setBalance(e.target.value)}
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div className="text-center">
                                        Date Search
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Date"
                                            value={date}
                                            onChange={(e) => setDate(e.target.value, 'date')}
                                        />
                                    </div>
                                </td>
                                <td>
                                    {' '}
                                    <div className="text-center">
                                        Select Users
                                        <select
                                            className="form-select"
                                            value={status}
                                            onChange={(e) => {
                                                setStatus(e.target.value, 'status');
                                            }}
                                        >
                                            <option value="">Select User</option>
                                            <option value="true">Active</option>
                                            <option value="false">Deactivated</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <p>
                        Showing {filterData == 0 ? 0 : initialShowData + 1} to {filterData.length == 199 ? lastShowData : filterData.length} of{' '}
                        {filterData.length == 199 ? filterData.length + 1 : filterData.length} entries
                    </p>
                </div>
                <div className="pages">
                    <div className="position-relative mb-5">
                        <nav aria-label="...">
                            <ul className="pagination pagination-sm position-absolute top-0 end-0">
                                <button className="btn btn-primary btn-sm me-3" onClick={() => setPages(pages > 1 ? pages - 1 : 1)}>
                                    Previous
                                </button>
                                {pageNumbers.map((number) => {
                                    return pages === number ? (
                                        <li className="page-item active" key={number}>
                                            <button aria-current={pages} className={number ? 'page-link' : 'page-link active'} onClick={() => setPages(number)}>
                                                {number}
                                            </button>
                                        </li>
                                    ) : (
                                        <li className="page-item" key={number}>
                                            <button aria-current={pages} className={number ? 'page-link' : 'page-link active'} onClick={() => setPages(number)}>
                                                {number}
                                            </button>
                                        </li>
                                    );
                                })}
                                <button className="btn btn-primary btn-sm ms-3" onClick={() => setPages(pages < pageNumbers.length ? pages + 1 : pages)}>
                                    Next
                                </button>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TableData;
