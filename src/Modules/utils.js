export const isValidArray = (data) => data && Array.isArray(data) && data.length > 0;
