import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LoginForm from './components/LoginForm';
import TableData from './components/TableData';

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/users-list">
                    <TableData />
                </Route>
                {}
                <Route path="/">
                    <LoginForm />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
